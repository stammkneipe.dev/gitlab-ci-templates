# Contributing

### Thank you for you interest in Gitlab CI Templates!  You are welcome here.

This Pipeline Project is an open source project and always open for outside contributors.

If you are looking for a place to start take a look at the [open issues](/issues).

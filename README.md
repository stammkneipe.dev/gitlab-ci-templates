# Gitlab CI Templates

> This Repository contains Pipeline Templates for various use-cases

## Pipelines

### Release

The [Release Pipeline](/.release.yml) uses [Semantic Release](https://github.com/semantic-release/semantic-release) to create Releases on a schedule. 

```yaml
include:
  - project: 'stammkneipe.dev/gitlab-ci-templates'
    ref: main
    file: '.release.yml'
```

### APKO

The [Apko Pipeline](/.apko.yml) combines [Melange](https://github.com/chainguard-dev/melange) and [Apko](https://github.com/chainguard-dev/apko) Pipelines to build better Docker Images.

```yaml
include:
  - project: 'stammkneipe.dev/gitlab-ci-templates'
    ref: main
    file: '.apko.yml'
```

## Contributing

[Contributing](/CONTRIBUTING.md)

## License

[MIT License](/LICENSE)

## Authors and acknowledgment

- [Patrick Domnick](https://gitlab.com/PatrickDomnick)
- [Henry Sachs](https://gitlab.com/DerAstronaut)
